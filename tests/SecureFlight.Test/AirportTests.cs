using AutoMapper;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;
using Moq;
using SecureFlight.Api.Controllers;
using SecureFlight.Api.Models;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;
using SecureFlight.Infrastructure;
using SecureFlight.Infrastructure.Repositories;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace SecureFlight.Test
{
    public class AirportTests
    {
        [Fact]
        public async Task Update_Succeeds()
        {
            //arrange
            var context = new Mock<SecureFlightDbContext>();

            var input = new AirportDataTransferObject()
            {
                City = "New York",
                Code = "123",
                Country = "USA",
                Name = "NY"
            };

            context.Setup(con => con.Entry(It.IsAny<object>())).Returns(new Mock<EntityEntry<Airport>>().Object);

            var controller = new AirportsController(new BaseRepository<Airport>(context.Object), 
                new Mock<IService<Airport>>().Object, new Mock<IMapper>().Object);

            await controller.Put(input);

            context.Verify(con => con.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once);
        }
    }
}
