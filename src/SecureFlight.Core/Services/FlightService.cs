﻿using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureFlight.Core.Services
{
    public class FlightService : BaseService<Flight>
    {
        public IRepository<Passenger> PassengerRepository { get; }
        public IRepository<PassengerFlight> PassengerFlightRepository { get; }

        public FlightService(IRepository<Flight> repository, 
            IRepository<Passenger> passengerRepository,
            IRepository<PassengerFlight> passengerFlightRepository) : base(repository)
        {
            PassengerRepository = passengerRepository;
            PassengerFlightRepository = passengerFlightRepository;
        }

    }
}
